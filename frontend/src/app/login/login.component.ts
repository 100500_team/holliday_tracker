import { Component } from "@angular/core";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Requests} from "./../services/requests.service";

@Component({
  selector: "login",
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [Requests]
})

export class LoginComponent{
  constructor(private requests: Requests) { }
  loginForm: FormGroup;

  ngOnInit(){
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    this.requests.GetUsers();
  }

  log(obj){
    this.requests.Login(obj.value).subscribe(
      data=>console.log(data)
    )
  }
}
