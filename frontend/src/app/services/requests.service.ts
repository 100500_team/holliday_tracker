import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()

export class Requests{
  BASE_URL: string = 'http://127.0.0.1:8080/api/v1/';
  mv = this;

  constructor(private http: HttpClient) { }

  Login(obj: object){
    let url = this.BASE_URL+"login";

    return this.http.post(url, obj, {responseType: 'json'});
  }

  GetUsers(){
    let url = this.BASE_URL+"user/all";

    this.http.get(url).subscribe(resp => console.log(resp));
  }

}
