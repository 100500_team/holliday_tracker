package com.ignite.tracker.service.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nik on 11/30/17.
 */
public class DateConverter {

    public static Date convertDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD-mm-yyyy");
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}
