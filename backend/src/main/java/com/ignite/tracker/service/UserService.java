package com.ignite.tracker.service;

import com.ignite.tracker.controller.common.dto.CreateUserRequest;
import com.ignite.tracker.entity.User;

import java.util.List;

/**
 * Created by nik on 11/28/17.
 */
public interface UserService {

    User getUserById(int id);
    List<User> getAllUsers();
    User createUser(CreateUserRequest createUserRequest);
    User checkUser(String email, String password);

}
