package com.ignite.tracker.service;

import com.ignite.tracker.controller.common.dto.CreateUserRequest;
import com.ignite.tracker.dao.UserDAO;
import com.ignite.tracker.domain.Role;
import com.ignite.tracker.domain.Status;
import com.ignite.tracker.entity.User;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.List;

import static com.ignite.tracker.service.common.DateConverter.convertDate;

/**
 * Created by nik on 11/28/17.
 */
@Service
public class UserServiceImpl implements UserService{

    private static final SecureRandom RANDOM = new SecureRandom();
    private static final int PASSWORD_LENGTH = 20;
    private static final int CHARSET_START = 33;
    private static final int CHARSET_END = 127;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Override
    public User getUserById(int id){
        User user = userDAO.getUserById(id);
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    @Override
    public User createUser(CreateUserRequest createUserRequest){

        User user = new User();

        user.setFirstName(createUserRequest.getFirstName());
        user.setLastName(createUserRequest.getLastName());
        user.setEmail(createUserRequest.getEmail());
        user.setRole(Role.get(createUserRequest.getUserRole()));
        user.setFirstDate(convertDate(createUserRequest.getFirstDate()));
        user.setManagerId(createUserRequest.getManagerId());
        user.setStatus(Status.get(createUserRequest.getStatus()));
        user.setPassword(generateRandomPassword());
        userDAO.createUser(user);

        return user;
    }

    private String generateRandomPassword(){
        String passord = RandomStringUtils.random(PASSWORD_LENGTH,
                CHARSET_START,
                CHARSET_END,
                false,false,null,
                RANDOM);
        LOGGER.info("Randomly generated password: {}", passord);
        return passord;
    }

    @Override
    public User checkUser(String email, String password){
        User user = userDAO.checkUser(email);
        return (user != null && user.getPassword()
                .equals(password)) ? user : null;
    }
}
