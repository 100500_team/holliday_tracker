package com.ignite.tracker.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.ignite.tracker.controller.common.dto.CreateUserRequest;

/**
 * Created by nik on 11/29/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreateUserRequest.class, name = "user")
})
public interface CreateRequest {
}
