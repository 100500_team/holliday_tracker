package com.ignite.tracker.dto;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by nik on 11/30/17.
 */
public class LoginRequest {

    /**
     * User Email
     */
    @NotNull
    @Email
    private String email;

    /**
     * User password
     */
    @NotNull
    @Size(min=6, message = "Password must contains at least 6 characters.")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


