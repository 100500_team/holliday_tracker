package com.ignite.tracker.controller;

import com.ignite.tracker.dto.LoginRequest;
import com.ignite.tracker.entity.User;
import com.ignite.tracker.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * Created by nik on 11/29/17.
 */
@Controller
@RequestMapping(RestAPIConstants.REST_API_PREFIX + RestAPIConstants.REST_API_VERSION + "/login")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<User> login(@Valid @RequestBody LoginRequest loginRequest){
        LOGGER.info("Trying to login for [{}]", loginRequest.getEmail());
        User user = userService.checkUser(loginRequest.getEmail(), loginRequest.getPassword());

        if(user != null){
            LOGGER.info("Login for [{}] succeeded", loginRequest.getEmail());
            return new ResponseEntity<User>(user, HttpStatus.OK);
        }
        else {
            LOGGER.info("Login for [{}] failed", loginRequest.getEmail());
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
