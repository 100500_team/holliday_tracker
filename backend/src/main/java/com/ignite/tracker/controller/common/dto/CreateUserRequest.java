package com.ignite.tracker.controller.common.dto;

import com.ignite.tracker.dto.CreateRequest;
import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by nik on 11/29/17.
 */
public class CreateUserRequest implements CreateRequest{

    /**
     * First name of the new user
     */
    @NotNull(message = "First name must not be empty.")
    @Size(min = 2, message = "First name must contain at least 2 characters.")
    private String firstName;

    /**
     * Last name of the new user
     */
    @NotNull(message = "Last name must not be empty.")
    @Size(min = 2, message = "Last name must contain at least 2 characters.")
    private String lastName;

    /**
     * Email of the new user
     */
    @NotNull(message = "Email must not be empty.")
    @Email
    private String email;

    /**
     * Role of the new user
     */
    @NotNull(message = "Role must not be empty.")
    private int userRole;

    @NotNull(message = "First date must not be empty.")
    private String firstDate;

    private Long managerId;

    @NotNull(message = "Status must not be null")
    private int status;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(String firstDate) {
        this.firstDate = firstDate;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
