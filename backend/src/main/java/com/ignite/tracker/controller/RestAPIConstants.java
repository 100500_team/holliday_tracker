package com.ignite.tracker.controller;

/**
 * Created by nik on 11/28/17.
 */
public class RestAPIConstants {

    public static final String REST_API_PREFIX = "/api";
    public static final String REST_API_VERSION = "/v1";

}
