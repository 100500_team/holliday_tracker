package com.ignite.tracker.controller;

import com.ignite.tracker.controller.common.dto.CreateUserRequest;
import com.ignite.tracker.dto.CreateRequest;
import com.ignite.tracker.entity.User;
import com.ignite.tracker.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by nik on 11/28/17.
 */
@Controller
@RequestMapping(RestAPIConstants.REST_API_PREFIX + RestAPIConstants.REST_API_VERSION + "/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Integer id) {
        LOGGER.info("Getting employee with ID {}", id);
        User user = userService.getUserById(id);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping("all")
    public ResponseEntity<List<User>> getAllUsers() {
        LOGGER.info("Getting all employees");
        List<User> list = userService.getAllUsers();
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    @PostMapping("create")
    public ResponseEntity createUser(@Valid @RequestBody CreateRequest createRequest) {
        LOGGER.info("Creating new user [{}]", createRequest);
        CreateUserRequest request = (CreateUserRequest) createRequest;

        User user = userService.createUser(request);

        return new ResponseEntity(HttpStatus.OK);
    }
}
