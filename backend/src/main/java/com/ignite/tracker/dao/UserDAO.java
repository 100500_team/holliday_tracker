package com.ignite.tracker.dao;

import com.ignite.tracker.controller.common.dto.CreateUserRequest;
import com.ignite.tracker.entity.User;

import java.util.List;

/**
 * Created by nik on 11/28/17.
 */
public interface UserDAO {

    List<User> getAllUsers();
    User getUserById(int id);
    void createUser(User user);
//    void updateUser(User user);

    User checkUser(String email);
}
