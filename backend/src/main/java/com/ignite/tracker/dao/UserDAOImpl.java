package com.ignite.tracker.dao;

import com.ignite.tracker.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by nik on 11/28/17.
 */
@Transactional
@Repository
public class UserDAOImpl implements UserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User getUserById(int id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public List<User> getAllUsers() {

        String hql = "from User ORDER BY id";

        return (List<User>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public void createUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public User checkUser(String email) {

        return (User) entityManager.createQuery("SELECT u FROM User u where u.email = :email")
                .setParameter("email", email).getSingleResult();
    }
}
