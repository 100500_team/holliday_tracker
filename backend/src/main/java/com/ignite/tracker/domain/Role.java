package com.ignite.tracker.domain;

/**
 * Created by nik on 11/30/17.
 */
public enum Role {

    ADMIN(0),
    DEVELOPER(1),
    MANAGER(2),
    UNDEFINED(-1);

    private int code;

    Role(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static Role get(int code) {
        if (code == 0) {
            return UNDEFINED;
        }
        for (Role role : Role.values()) {
            if (code == role.getCode()) {
                return role;
            }
        }
        return UNDEFINED;
    }

}
