package com.ignite.tracker.domain;

/**
 * Created by nik on 11/30/17.
 */
public enum  Status {
    INACTIVE(0),
    ACTIVE(1),
    UNDEFINED(-1);


    private int code;

    Status(int code) { this.code = code; }

    public int getCode() {
        return code;
    }

    public static Status get(int code) {
        for (Status status : Status.values()) {
            if(code == status.getCode()) {
                return status;
            }
        }
        return UNDEFINED;
    }
}
